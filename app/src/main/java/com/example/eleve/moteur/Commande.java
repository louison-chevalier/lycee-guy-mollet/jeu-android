package com.example.eleve.moteur;

/**
 * Created by Louison on 04/05/2017.
 */

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;


/**
 * Created by julienlegales on 03/02/2017.
 */
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;

import com.example.eleve.ppe2.Barrevie;
import com.example.eleve.ppe2.Niveau;
import com.example.eleve.ppe2.Niveau1;

import java.io.IOException;
import java.io.InputStream;

// La classe Joystick permet d'afficher un Joystick si on l'active dans le niveau
public class Commande {
    Rect r;
    Clip image3;
    Bitmap bitmap;
    Rect resolution;
    Niveau niveau;
    Bouton bta, bth;

    public Commande(AssetManager assetManager, int x, int y, int x2, int y2, Rect resolution, Niveau niveau) {

        this.resolution = resolution;
        this.niveau = niveau;


        r = new Rect(x,y,x2,y2);
        bth = new Bouton(Math.round(x+0*niveau.getCoeffx()),Math.round(y+60*niveau.getCoeffy()), Math.round(x+60*niveau.getCoeffx()), Math.round(y+100*niveau.getCoeffy()));
        bta = new Bouton(Math.round(x+65*niveau.getCoeffx()),Math.round(y+0*niveau.getCoeffy()), Math.round(x+125*niveau.getCoeffx()), Math.round(y+60*niveau.getCoeffy()));
        InputStream inputStream = null;

        try {
            inputStream = assetManager.open("4.png");
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_4444;
            bitmap = BitmapFactory.decodeStream(inputStream, null,options);
            inputStream.close();
            image3 = new Clip(bitmap);
            image3.ajouterImage(0, 0, 341, 299);
            image3.zoom(1,1);


        }

        catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void affiche(Canvas canvas){
        image3.affiche(r.left, r.top, canvas);
    }

    // Si j'appuie sur le tactile
    public void toucherDown(float x, float y){
        Log.d("bjr","bjr");
        if (bta.toucher(x, y)){
            /*niveau.getRenderView().etatdujeu();*/
            Log.d("btp","btp");

        }

    }


    // Si je relâche le tactile
    public void toucherUp(float x, float y){
        if (bta.toucher(x, y)){
            Log.d("btp","btp");
        }


    }


}

