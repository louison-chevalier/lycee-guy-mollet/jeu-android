package com.example.eleve.moteur;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;

import com.example.eleve.ppe2.Niveau;
import com.example.eleve.ppe2.Niveau1;

import java.io.IOException;
import java.io.InputStream;


/**
 * Created by julienlegales on 03/02/2017.
 */
// La classe Joystick permet d'afficher un Joystick si on l'active dans le niveau
public class Joystick {
    Rect r;
    Clip image;
    Bitmap bitmap;
    Bouton btHaut, btBas, btGauche, btDroit;
    Rect resolution;
    Niveau niveau;

    public Joystick(AssetManager assetManager, int x, int y, int x2, int y2, Rect resolution, Niveau niveau) {

        this.resolution = resolution;

        this.niveau = niveau;

        x = Math.round(x*niveau.getCoeffx());

        y = 700;

        r = new Rect(x,y,x2,y2);

        btHaut = new Bouton(Math.round(x+50*niveau.getCoeffx()), Math.round(y), Math.round(x+111*niveau.getCoeffx()), Math.round(y+50*niveau.getCoeffy()));
        btBas = new Bouton(Math.round(x+50*niveau.getCoeffx()),Math.round(y+111*niveau.getCoeffy()), Math.round(x+111*niveau.getCoeffx()), Math.round(y+160*niveau.getCoeffy()));
        btGauche = new Bouton(Math.round(x+0*niveau.getCoeffx()), Math.round(y+50*niveau.getCoeffy()), Math.round(x+50*niveau.getCoeffx()),Math.round( y+111*niveau.getCoeffy()));
        btDroit = new Bouton(Math.round(x+110*niveau.getCoeffx()), Math.round(y+50*niveau.getCoeffy()), Math.round(x+160*niveau.getCoeffx()), Math.round(y+111*niveau.getCoeffy()));
        InputStream inputStream = null;


        try {
            inputStream = assetManager.open("flatLight07.png");
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_4444;
            bitmap = BitmapFactory.decodeStream(inputStream, null,options);
            inputStream.close();
            image = new Clip(bitmap);
            image.ajouterImage( 0, 0, 160, 160);
            image.zoom(2,2);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void affiche(Canvas canvas){
        image.affiche(r.left, r.top, canvas);
    }

    // Si j'appuie sur le tactile
    public void toucherDown(float x, float y){
       // Touche haut
       if (btHaut.toucher(x, y)){
           niveau.getHeros().hautDown();
       }
       // Touche bas
       if (btBas.toucher(x, y)){
           niveau.getHeros().basDown();
       }
       // Touche gauche
       if (btGauche.toucher(x, y)){
           niveau.getHeros().gaucheDown();
       }
       // Touche droite
       if (btDroit.toucher(x, y)){
           niveau.getHeros().droiteDown();
       }
    }

    // Si je relâche le tactile
    public void toucherUp(float x, float y){
        // Touche haut
        if (btHaut.toucher(x, y)){
            niveau.getHeros().hautUp();
        }
        // Touche bas
        if (btBas.toucher(x, y)){
            niveau.getHeros().basUp();
        }
        // Touche gauche
        if (btGauche.toucher(x, y)){
            niveau.getHeros().gaucheUp();
        }
        // Touche droite
        if (btDroit.toucher(x, y)){
            niveau.getHeros().droiteUp();

        }
    }
}
