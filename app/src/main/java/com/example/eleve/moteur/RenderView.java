package com.example.eleve.moteur;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.eleve.ppe2.Niveau;
import com.example.eleve.ppe2.Niveau1;
import com.example.eleve.ppe2.Niveau2;


/**
 * Created by eleve on 03/02/17.
 */


public class RenderView extends SurfaceView implements Runnable {

    SurfaceHolder holder;
    Thread renderThread = null;
    Context context;
    Canvas canvas;
    long temps;

    public char etatjeu;

    Rect resolution;
    float tickTime = 0;
    static final float TICK_INITIAL = 0.1f;
    float tick = TICK_INITIAL;
    volatile boolean running = false;

    AssetManager assetManager;
    Boolean init;

    Niveau niveau1;
    Niveau2 niveau2;

    public RenderView(Context context) {
        super(context);
        this.context = context;
        holder = getHolder();
        init = false;
        etatjeu = 'm';
        temps = System.currentTimeMillis();
    }

    // Lorsque le processus du jeu tourne
    @Override
    public void run() {
        long startTime = System.nanoTime();

        while (running) {
            if (!holder.getSurface().isValid())
                continue;
            float deltaTime = (System.nanoTime() - startTime) / 1000000000.0f;
            startTime = System.nanoTime();

            // Blocage de la surface pour dessiner dessus
            canvas = holder.lockCanvas();

            canvas.drawRGB(0, 0, 0);
            if (!init){
                if (canvas.getWidth()==800) {
                    resolution = new Rect(0, 0, canvas.getWidth(), canvas.getHeight());
                }
                else{
                    // Créé la résolution d'une machine virtuelle
                    resolution = new Rect(0, 0, 800, 651);
                }
                init(resolution);
                init = true;
            }
            // Met à jour les données du jeu
            update(deltaTime);
            // Affiche le jeu
            affiche();
            holder.unlockCanvasAndPost(canvas);
        }
    }



    // Met à jour les données du jeu
    public void update(float deltaTime) {

        tickTime += deltaTime;
        while (tickTime > tick) {
            tickTime -= tick;
            // Appel la méthode qui met à jour les données du jeu
            modifier(deltaTime);
        }


    }



    // Lorsque l'activity est à l'écran
    public void resume() {
        running = true;
        renderThread = new Thread(this);
        renderThread.start();

    }


    // Lorsque l'activity n'est plus à l'écran
    public void pause() {

        running = false;
        while (true) {
            try {
                renderThread.join();
                return;
            } catch (InterruptedException e) {

            }
        }
    }

    // Gère les touches lorsqu'on appuie dessus
    public void toucherDown(float x, float y){
        niveau2.toucherDown(x, y);
        niveau1.toucherDown(x, y);

    }
    // Gère les touches lorsqu'on r
    public void toucherUp(float x, float y){
        niveau2.toucherUp(x, y);
        niveau1.toucherUp(x, y);

    }

    // Initialise toutes les données du jeu
    public void init(Rect resolution)  {
        assetManager = context.getAssets();
        niveau2 = new Niveau2(assetManager, resolution, canvas);
        niveau1 = new Niveau1(assetManager, resolution, canvas);




    }

    // Modifie les données du jeu
    public void modifier(float deltatime){
        niveau1.bouger(deltatime);

    }

    //  Affiche le jeu
    public void affiche(){


        if (etatjeu =='m'){
            niveau2.afficher();
            if(temps+5000 < System.currentTimeMillis()){
                etatjeu = 'j';
            }
        }

        if (etatjeu =='j'){
            niveau1.afficher();
        }


    }
    // Gère le clavier
    public void clavier(KeyEvent event){
        niveau2.clavier(event);
        niveau1.clavier(event);

    }



}

