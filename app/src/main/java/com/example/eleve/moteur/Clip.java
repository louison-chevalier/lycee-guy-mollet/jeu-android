package com.example.eleve.moteur;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

import java.util.LinkedList;

/**
 * Created by julienlegales on 03/02/2017.
 */
public class Clip {
    private LinkedList<Rect> liste;
    private LinkedList<Rect> listeCollision;
    private Bitmap image;
    private int indice;
    private int debutBoucle;
    private boolean boucle;
    private boolean fonctionne;
    private boolean commencer;
    private float xZoom;
    private float yZoom;
    Matrix tourneGauche = new Matrix();
    Rect dest;
    private char direction;

    // Initialisation d'un clip en recevant en paramètre une image
    public Clip(Bitmap image){
        this.image = image;
        init();
    }
    // Permet de zoomer le clip avec une valeur en x et une valeur en y
    public void zoom(float x, float y){
       this.xZoom = x;
       this.yZoom = y;
    }
    // Permet d'ajouter une image dans le clip en donnant ses coordonnées
    public void ajouterImage(int x1,int y1, int x2, int y2){
        this.liste.add(new Rect(x1,y1,x2,y2));
    }
    // Ajouter un rectangle de collision en donnant 2 points
    public void ajouterCollision(int x1,int y1, int x2, int y2){
        this.listeCollision.add(new Rect(x1,y1,x2,y2));

    }

    // Ajouter un rectangle de collision
    public void ajouterCollision(Rect r){

        this.listeCollision.add(r);
    }

    // Change d'image dans le clip
    public void next(){
        if (commencer) {
            if (this.fonctionne) {
                if (this.indice < this.liste.size() - 1) {
                    this.indice++;
                } else {

                    if (this.boucle) {
                        indice = debutBoucle;
                    } else {

                        this.fonctionne = false;
                    }

                }
            }
        }
        else{
            commencer = true;

        }
    }
    // Affiche le clip
    public void affiche(int posX, int posY, Canvas canvas ){
        int x2,y2;

            x2 = Math.round(posX + liste.get(indice).width() * this.xZoom);
            y2 = Math.round(posY + liste.get(indice).height() * this.yZoom);

            dest = new Rect(posX, posY, x2, y2);

        canvas.drawBitmap(image, liste.get(indice), dest, null);



    }
    // Initialise les valeur du clip
    public void init(){
        this.liste = new LinkedList<Rect>();
        this.listeCollision = new LinkedList<Rect>();

        this.indice = 0;
        this.debutBoucle =0;
        this.boucle = false;
        this.fonctionne = true;
        this.xZoom = 1;
        this.yZoom = 1;
        this.dest=new Rect(0,0,0,0);
        this.commencer=false;
        this.direction = 'd';
    }
    // Est ce que le clip est en boucle?
    public boolean isBoucle() {
        return boucle;
    }
   // Mettre en boucle ou non
    public void setBoucle(boolean boucle) {
        this.boucle = boucle;
    }
    // Retourne la liste des coordonnées des images
    public LinkedList<Rect> getListe() {
        return liste;
    }
    // Permet de définir une liste de coordonnées
    public void setListe(LinkedList<Rect> liste) {
        this.liste = liste;
    }
    // Récupère l'image du clip
    public Bitmap getImage() {
        return image;
    }
    // Définit l'image du clip
    public void setImage(Bitmap image) {
        this.image = image;
    }
    // Retourne l'indice courant du clip
    public int getIndice() {
        return indice;
    }
    // assigner un indice
    public void setIndice(int indice) {
        this.indice = indice;
    }
    // récupère l'indice du début de boucle
    public int getDebutBoucle() {
        return debutBoucle;
    }
    // définit le début de la boucle
    public void setDebutBoucle(int debutBoucle) {
        this.debutBoucle = debutBoucle;
    }
    // Inverse les coordonnées vers la gauche
    public void inverseCoordonnees(){

        tourneGauche.setScale(-1, 1);
        image = Bitmap.createBitmap(image, 0, 0,
                image.getWidth(), image.getHeight(), tourneGauche, true);
        int tailleImage = image.getWidth();
        int tailleTableau = liste.size();

        LinkedList<Rect> liste2 = new LinkedList<Rect>();
        for (Rect r : liste){
            liste2.add(new Rect(tailleImage-r.right,r.top,tailleImage-r.left,r.bottom));
        }
        liste = liste2;
        direction = 'g';

    }
    // Récupère la longueur de l'image courante
    public int getLongueur(){
        return Math.round(liste.get(indice).width()*this.xZoom);
    }
    // Retourne le rectangle de collision par rapport à sa position sur la scène
    public Rect getRectCollision(int posX,int posY){
        int x2,y2;
        if (listeCollision.size()!=0) {
            int x1=Math.round(posX + listeCollision.get(indice).left*this.xZoom);
            int y1=Math.round(posY+listeCollision.get(indice).top*this.yZoom);

            x2 = Math.round(x1+(listeCollision.get(indice).width())* this.xZoom);
            y2=Math.round(y1 +listeCollision.get(indice).height() * this.yZoom);
            dest = new Rect(x1,y1,x2+listeCollision.get(indice).left,y2);
        }
        else{
            x2 = Math.round(posX + liste.get(indice).width() * this.xZoom);
            y2 = Math.round(posY + liste.get(indice).height() * this.yZoom);
            dest = new Rect(posX,posY,x2,y2);
        }


        return dest;
    }
    // Retourne le rectangle de collision
    public Rect getRectCollisionReal(){
        Rect dest;
        if (listeCollision.size()!=0) {
            dest = new Rect(Math.round(listeCollision.get(indice).left*this.xZoom),Math.round(listeCollision.get(indice).top*this.yZoom),Math.round(listeCollision.get(indice).width()*this.xZoom),Math.round(listeCollision.get(indice).height()*this.yZoom));
        }
        else{
            dest = new Rect(liste.get(indice).left,liste.get(indice).top,Math.round(liste.get(indice).width()*this.xZoom),Math.round(liste.get(indice).height()*this.yZoom));
        }
        return dest;
    }
    // Affiche le rectangle de collision pour debugguer
    public void afficheCollisionDebug(int posX, int posY, Canvas canvas){
        Rect r;
        Paint p;
        r = getRectCollision(100, 100);
        p = new Paint();
        p.setStyle(Paint.Style.FILL);
        p.setColor(Color.BLUE);
        canvas.drawRect(r,p);
    }
    //Juste la taille du rectangle sans sa position dans le jeu
    public Rect getRect(){
        int x2,y2;

        x2 = Math.round(liste.get(indice).width()*this.xZoom);
        y2 = Math.round(liste.get(indice).height()*this.yZoom);

        dest = new Rect(0,0,x2,y2);
        return dest;
    }
    // Est ce que le clip est terminé?
    public boolean isFonctionne() {
        return fonctionne;
    }

    // Définit le fonctionnement du clip
    public void setFonctionne(boolean fonctionne) {

    }
    // Est ce que le clip a commencé
    public boolean isCommencer() {
        return commencer;
    }

    // Définit si un clip est commencé ou pas
    public void setCommencer(boolean commencer) {
        indice=0;
        this.fonctionne=true;
        this.commencer = commencer;
    }

    public void Zoom(int i, int i1) {
    }

    public void deplace() {
    }

    public void inverseCordonnees() {
    }

    public void affichage(int left, int top, Canvas canvas) {
    }
}

