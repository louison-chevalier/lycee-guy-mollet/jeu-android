package com.example.eleve.moteur;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by eleve on 03/02/17.
 */
public class Chrono {
    private int heure;
    private int min;
    private int seconde;
    private long startTime;
    private int mms;
    private Float diff;
    private long time;
    private Float minutes;
    private Float secondes;
    private String affSecondes;
    private String affMinutes;
    private Rect resolution;

    /*    Constructeur de Chrono afin d'initialiser toutes les variables à 0 */
    public Chrono(Rect resolution){
        // Initialisation de toutes les variables
        mms=0;
        heure=0;
        min=0;
        seconde=0;
        // récupère l'heure du système
        startTime = System.nanoTime();
        this.resolution = resolution;

    }

    public void update(){
        time = System.nanoTime()-startTime;
        diff= time / (1000000000.0f);
        minutes = diff/60;
        min = minutes.intValue();
        secondes = diff%60;
        seconde = secondes.intValue();

    }



    /*  Affiche le temps après avoir défini la taille et la couleur du texte   */
    public void affiche(Canvas canvas, int x, int y){
        float coeffHauteur= this.resolution.height()/561;
        float coeffLargeur = this.resolution.width()/800;
        Paint p = new Paint();
        p.setStyle(Paint.Style.STROKE);
        p.setColor(Color.WHITE);
        p.setTextSize(30*coeffHauteur);



        // Si les secondes sont inférieures à 10 on rajoute un O devant
        if (seconde<10){
            affSecondes =  '0'+String.valueOf(seconde);
        }
        else {
            affSecondes=String.valueOf(seconde);
        }
        //Si les minutes sont inférieures à 10 alors on rajoute un 0 devant
        if (min<10){
            affMinutes = '0'+String.valueOf(min);
        }
        else{
            affMinutes = String.valueOf(min);
        }
        canvas.drawText( affMinutes+ ':' +affSecondes , x*coeffLargeur, y*coeffHauteur, p);

    }
}