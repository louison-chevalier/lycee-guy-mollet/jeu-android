package com.example.eleve.moteur;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;

import com.example.eleve.ppe2.Barrevie;
import com.example.eleve.ppe2.Niveau;
import com.example.eleve.ppe2.Niveau1;

import java.io.IOException;
import java.io.InputStream;


public class Action {
    Rect r;
    Clip image2;
    Bitmap bitmap;
    Rect resolution;
    Niveau niveau;
    Bouton bta, btb;

    public Action(AssetManager assetManager, int x, int y, int x2, int y2, Rect resolution, Niveau niveau) {

        this.resolution = resolution;
        this.niveau = niveau;

        r = new Rect(x,y,x2,y2);
        bta = new Bouton(Math.round(x+0*niveau.getCoeffx()),Math.round(y+60*niveau.getCoeffy()), Math.round(x+60*niveau.getCoeffx()), Math.round(y+100*niveau.getCoeffy()));
        btb = new Bouton(Math.round(x+65*niveau.getCoeffx()),Math.round(y+0*niveau.getCoeffy()), Math.round(x+125*niveau.getCoeffx()), Math.round(y+60*niveau.getCoeffy()));
        InputStream inputStream = null;

        try {
            inputStream = assetManager.open("2.png");
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_4444;
            bitmap = BitmapFactory.decodeStream(inputStream, null,options);
            inputStream.close();
            image2 = new Clip(bitmap);
            image2.ajouterImage(0, 0, 341, 299);
            image2.zoom(1,1);

        }

        catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void affiche(Canvas canvas){
        image2.affiche(r.left, r.top, canvas);
    }

    // Si j'appuie sur le tactile
    public void toucherDown(float x, float y){
        if (bta.toucher(x, y)){
            niveau.getHeros().coupDown();
            Log.d("bta","bta");
        }
        if (btb.toucher(x, y)){
            niveau.getBarrevie().actionvie();

        }

    }


    // Si je relâche le tactile
    public void toucherUp(float x, float y){
        if (bta.toucher(x, y)){
            niveau.getHeros().coupUp();
        }
        if (btb.toucher(x, y)){
            niveau.getBarrevie().actionvie();
        }

    }
}
