package com.example.eleve.moteur;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by julienlegales on 03/02/2017.
 */
public class Bouton {
    Rect r;
    Paint p;
    public Bouton(int x, int y, int x2, int y2){
       r = new Rect(x, y, x2, y2);
       p = new Paint();
       p.setStyle(Paint.Style.FILL);
       p.setColor(Color.BLUE);
    }
    public void afficher(Canvas canvas){
        canvas.drawRect(r, p);
    }

    public boolean toucher(float x, float y){
        if(r.left>x){
            return false;
        }
        if(r.right<x){
            return false;
        }
        if(r.top>y){
            return false;
        }
        if(r.bottom<y){
            return false;
        }
        return true;
    }

}
