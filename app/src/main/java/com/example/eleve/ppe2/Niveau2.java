package com.example.eleve.ppe2;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.KeyEvent;

import com.example.eleve.moteur.Chrono;

/**
 * Created by Louison on 03/05/2017.
 */

public class Niveau2 extends Niveau{

    Chrono chrono;
    AssetManager assetManager;
    Rect resolution;

    Canvas canvas;
    Rect taille; // Taille du niveau

    float coeffx, coeffy;
    float coeffHauteur;
    float coeffLargeur;
    Bitmap rendu;
    Canvas canvasRendu;
    Decors2 decors2;

    public char etatjeu;

    public Niveau2(AssetManager assetManager, Rect resolution, Canvas canvas){
        this.assetManager = assetManager;
        this.resolution = resolution;
        this.canvas = canvas;


        if (this.resolution.height()==561){
            coeffHauteur= this.resolution.height()/561;
            coeffLargeur = this.resolution.width()/800;
            coeffx = coeffLargeur;
            coeffy = coeffHauteur;

        }else{
            coeffHauteur= 2.5f;
            coeffLargeur = 2.5f;
            coeffx = coeffLargeur;
            coeffy = coeffHauteur;
        }


        this.taille = new Rect(0,0,Math.round(2560),Math.round(1000));
        this.initialiser();
        rendu = Bitmap.createBitmap(Math.round(this.resolution.width()*coeffx), Math.round(this.resolution.height() * coeffy), Bitmap.Config.RGB_565);
        this.canvasRendu = new Canvas(rendu);


    }

    public void initialiser(){
        chrono = new Chrono(resolution);
        decors2 = new Decors2(assetManager, this);
    }

    @Override
    public void bouger(float deltatime) {

    }


    protected void texte(Canvas canvas){
        Paint p = new Paint();
        p.setStyle(Paint.Style.STROKE);
        p.setColor(Color.WHITE);
        p.setTextSize(30);
        canvas.drawText("BETA", 80, 80, p);
    }

    public void afficher(){
        canvasRendu.drawRGB(0, 0, 128);
        decors2.afficher(canvasRendu);
        texte(canvasRendu);

        canvas.drawBitmap(rendu, 0, 0, null);
    }



    @Override
    public void toucherDown(float x, float y) {

    }

    @Override
    public void toucherUp(float x, float y) {

    }

    @Override
    public void clavier(KeyEvent event) {

    }

    @Override
    public float getCoeffx() {
        return 0;
    }

    @Override
    public float getCoeffy() {
        return 0;
    }

    @Override
    public Heros getHeros() {
        return null;
    }

    @Override
    public Barrevie getBarrevie() {
        return null;
    }

    @Override
    public void setBarrevie(Barrevie barrevie) {

    }


}


