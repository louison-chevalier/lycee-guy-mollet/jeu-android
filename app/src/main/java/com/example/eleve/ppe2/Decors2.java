package com.example.eleve.ppe2;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.example.eleve.moteur.Clip;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by julienlegales on 09/02/2017.*
 **/

public class Decors2 {

    Niveau niveau;

    /*FOND*/
    Clip decors2;
    Bitmap z;


    // Initialise le decors
    public Decors2(AssetManager assetManager, Niveau niveau){

        this.niveau = niveau;
        try {
            /*permet de créer un flux*/
            InputStream inputStream = assetManager.open("titlescreen.png");

            /*option de l'image*/
            BitmapFactory.Options options= new BitmapFactory.Options();

             /*on ne gere pas les pixels transparents ici*/
            options.inPreferredConfig = Bitmap.Config.RGB_565;

            /*charger l'image dans les décors en gérant les options*/
            z= BitmapFactory.decodeStream(inputStream, null, options);
            decors2= new Clip(z);
            decors2.ajouterImage(0,0,256,144);
            decors2.zoom(8,8);



        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // Afficher le décors
    public void afficher(Canvas canvas){

        decors2.affiche(0,0,canvas);


    }
    // Déplacer les éléments du décors
    public void bouge(){


    }

    // Est ce que 2 rectangles sont en collision
    public boolean toucher( Rect c, Rect a)
    {

        int leftA;
        int rightA;
        int topA;
        int bottomA;


        leftA = a.left;
        rightA = a.right;
        topA = a.top;
        bottomA = a.bottom;

        if( bottomA <= c.top )
        {
            return false;
        }

        if( topA >= c.bottom )
        {
            return false;
        }

        if( rightA <= c.left)
        {
            return false;
        }

        if( leftA >= c.right )
        {
            return false;
        }


        return true;
    }

}
