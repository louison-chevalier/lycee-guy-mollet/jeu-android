package com.example.eleve.ppe2;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.KeyEvent;


import com.example.eleve.moteur.Chrono;
import com.example.eleve.moteur.Joystick;
import com.example.eleve.moteur.Action;


/**
 * Created by julienlegales on 19/02/2017.
 */

// Niveau 1 permet de créer le premier niveau de notre jeu

public class Niveau1 extends Niveau{

    Chrono chrono;
    Joystick joystick;
    Boolean affJoystick;
    AssetManager assetManager;
    Decors decors;
    Rect resolution;
    Heros heros;
    Barrevie barrevie;

    Canvas canvas;
    Rect taille; // Taille du niveau

    float coeffx, coeffy;
    float coeffHauteur;
    float coeffLargeur;
    Bitmap rendu;
    Canvas canvasRendu;
    Action action;

    public Niveau1(AssetManager assetManager, Rect resolution, Canvas canvas){
        this.assetManager = assetManager;
        this.resolution = resolution;
        this.canvas = canvas;


        if (this.resolution.height()==561){
            coeffHauteur= this.resolution.height()/561;
            coeffLargeur = this.resolution.width()/800;
            coeffx = coeffLargeur;
            coeffy = coeffHauteur;

        }else{
            coeffHauteur= 2.5f;
            coeffLargeur = 2.5f;
            coeffx = coeffLargeur;
            coeffy = coeffHauteur;
        }


        this.taille = new Rect(0,0,Math.round(2560),Math.round(1000));
        this.initialiser();
        rendu = Bitmap.createBitmap(Math.round(this.resolution.width()*coeffx), Math.round(this.resolution.height() * coeffy), Bitmap.Config.RGB_565);
        this.canvasRendu = new Canvas(rendu);


    }

    public void initialiser(){
        chrono = new Chrono(resolution);
        decors = new Decors(assetManager, this);

        affJoystick = true;
        joystick = new Joystick(assetManager,30,215,160,560,resolution, this);
        action = new Action(assetManager,1400,670,1800,1070,resolution, this);
        barrevie =  new Barrevie(assetManager, 30,215,160,560, resolution, this);
        heros = new Heros(assetManager, resolution, decors, this);

    }

    public void bouger(float deltatime){
        chrono.update();
        canvas.drawRGB(120, 120, 120);
        decors.bouge();
        heros.bouge(deltatime);

    }

    protected void texte(Canvas canvas){
        Paint p = new Paint();
        p.setStyle(Paint.Style.STROKE);
        p.setColor(Color.WHITE);
        p.setTextSize(30);
        canvas.drawText("BETA", 80, 80, p);
    }

    public void afficher(){
        canvasRendu.drawRGB(0, 0, 128);
        chrono.affiche(canvasRendu, 700, 30);
        decors.afficher(canvasRendu);
        heros.afficher(canvasRendu);
        action.affiche(canvasRendu);
        joystick.affiche(canvasRendu);
        barrevie.afficher(canvasRendu);
        texte(canvasRendu);
        canvas.drawBitmap(rendu, 0, 0, null);
    }

    public void toucherDown(float x, float y){
        joystick.toucherDown(x, y);
        action.toucherDown(x, y);

    }

    public void toucherUp(float x, float y){
        joystick.toucherUp(x, y);
        action.toucherUp(x,y);
    }

    public void clavier(KeyEvent event){
        if (event.getAction()== KeyEvent.ACTION_DOWN){

            //Haut
            if(event.getKeyCode()==19){
                heros.hautDown();
            }
            // Bas
            if(event.getKeyCode()==20){
                heros.basDown();

            }
            // Gauche
            if(event.getKeyCode()==21){
                heros.gaucheDown();

            }
            // Droite
            if(event.getKeyCode()==22){
                heros.droiteDown();

            }


        }
        if (event.getAction()== KeyEvent.ACTION_UP){
            if(event.getKeyCode()==KeyEvent.KEYCODE_D){
                //renderView.afficherDebug();
            }
            if(event.getKeyCode()==19){
                heros.hautUp();

            }
            if(event.getKeyCode()==20){
                heros.basUp();

            }
            if(event.getKeyCode()==21){
                heros.gaucheUp();

            }
            if(event.getKeyCode()==22){
                heros.droiteUp();

            }
            // Saut
            if(event.getKeyCode()==KeyEvent.KEYCODE_SPACE){


            }
        }
    }

    public Rect getTaille() {
        return taille;
    }

    public void setTaille(Rect taille) {
        this.taille = taille;
    }

    public Heros getHeros() {
        return heros;
    }

    public void setHeros(Heros heros) {
        this.heros = heros;
    }

    public Rect getResolution() {
        return resolution;
    }

    public void setResolution(Rect resolution) {
        this.resolution = resolution;
    }

    public float getCoeffx() {
        return coeffx;
    }

    public void setCoeffx(int coeffx) {
        this.coeffx = coeffx;
    }

    public float getCoeffy() {
        return coeffy;
    }

    public void setCoeffy(int coeffy) {
        this.coeffy = coeffy;
    }

    public Barrevie getBarrevie() {return barrevie;}

    public void setBarrevie(Barrevie barrevie) { this.barrevie = barrevie;
    }
}

