package com.example.eleve.ppe2;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;

import com.example.eleve.moteur.Clip;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

public class Barrevie {


    /*BARRE DE VIE*/
    Bitmap c;
    Clip linkbarredevie;
    int linkvie =4;
    char etatlinkvie;



    public Barrevie(AssetManager assetManager,  int x, int y, int x2, int y2 , Rect resolution , Niveau niveau) {

        x=400;
        y=400;

        try {
            /*permet de créer un flux*/
            InputStream inputStream = assetManager.open("HUD.png");

            /*option de l'image*/
            BitmapFactory.Options options= new BitmapFactory.Options();

             /*on ne gere pas les pixels transparents ici*/
            options.inPreferredConfig = Bitmap.Config.RGB_565;

            /*charger l'image dans les décors en gérant les options*/
            c= BitmapFactory.decodeStream(inputStream, null, options);

            etatlinkvie = 'b';

            linkbarredevie = new Clip(c);
            linkbarredevie.zoom(7,7);
            linkbarredevie.ajouterImage(161, 24, 187, 31);
            linkbarredevie.ajouterImage(161, 24, 187, 31);

            linkbarredevie.ajouterImage(161, 34, 187, 41);
            linkbarredevie.ajouterImage(161, 34, 187, 41);

            linkbarredevie.ajouterImage(161, 44, 187, 51);
            linkbarredevie.ajouterImage(161, 44, 187, 51);

            linkbarredevie.ajouterImage(161, 54, 187, 61);
            linkbarredevie.ajouterImage(161, 54, 187, 61);

            linkbarredevie.setBoucle(true);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void afficher (Canvas canvas) {


        // BARRE VIE
        if (etatlinkvie == 'b'){
            linkvie = linkvie-1;
            linkbarredevie.affiche(1650, 80, canvas);
            linkbarredevie.next();
            etatlinkvie = 'a';
        }


        if (etatlinkvie == 'a'){
            linkbarredevie.affiche(1650, 80, canvas);
        }


    }



    public void actionvie(){
        etatlinkvie ='b';
    }
}
