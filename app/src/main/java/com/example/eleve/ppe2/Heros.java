package com.example.eleve.ppe2;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;

import com.example.eleve.moteur.Clip;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

public class Heros {

    Rect resolution;
    Decors decors;
    Niveau niveau;


    //EMPLACEMENT INITIAL DE LINK
    int x, y;
    int ax=380, ay=450;


    //ETAT DE LINK
    char etat;


    //CLIP DEPLACEMENT
    Bitmap b;
    Clip linkdroit;
    Clip linkgauche;
    Clip linkbas;
    Clip linkhaut;


    //CLIP ATTAQUE
    Clip linkattaquebas;
    Clip linkattaquedroit;
    Clip linkattaquegauche;
    Clip linkattaquehaut;


    //CLIP VIE
    Bitmap f;
    Clip life;


    //CLIP OBJET
    Bitmap  e;
    Clip hude;



   //Met en mémoire un héros et reçoit une résolution, un decors et un niveau
    public Heros(AssetManager assetManager, Rect resolution, Decors decors, Niveau niveau) {
        // On garde les informations sur la résolution
        this.resolution = resolution;
        // On garde les informations sur le décors
        this.decors = decors;
        // On garde les informations sur le niveau
        this.niveau = niveau;


        try {
            /*permet de créer un flux*/
            InputStream inputStream = assetManager.open("link.png");

            /*option de l'image*/
            BitmapFactory.Options options= new BitmapFactory.Options();

             /*on ne gere pas les pixels transparents ici*/
            options.inPreferredConfig = Bitmap.Config.RGB_565;

            /*charger l'image dans les décors en gérant les options*/
            b= BitmapFactory.decodeStream(inputStream, null, options);

            //ETAT DE DEPART DE LINK
            etat = 'n';

            //LINK DROIT
            linkdroit = new Clip(b);
            linkdroit.zoom(6,5);
            linkdroit.ajouterImage(785, 126, 805, 152);
            linkdroit.ajouterImage(808, 126, 828, 152);
            linkdroit.ajouterImage(833, 126, 853, 152);
            linkdroit.ajouterImage(855, 126, 875, 152);
            linkdroit.ajouterImage(879, 126, 899, 152);
            linkdroit.ajouterImage(903, 126, 923, 152);
            linkdroit.ajouterImage(925, 126, 945, 152);
            linkdroit.setBoucle(true);

            linkattaquedroit = new Clip(b);
            linkattaquedroit.zoom(6,5);
            linkattaquedroit.ajouterImage(689, 431, 720, 462);
            linkattaquedroit.ajouterImage(722, 431, 753, 462);
            linkattaquedroit.ajouterImage(755, 431, 786, 462);
            linkattaquedroit.ajouterImage(806, 431, 837, 462);
            linkattaquedroit.ajouterImage(858, 431, 889, 462);
            linkattaquedroit.ajouterImage(900, 431, 931, 462);
            linkattaquedroit.ajouterImage(945, 431, 976, 462);
            linkattaquedroit.ajouterImage(991, 431, 1022, 462);
            linkattaquedroit.ajouterImage(1031, 431, 1062, 462);
            linkattaquedroit.setBoucle(true);


            //LINK GAUCHE
            linkgauche = new Clip(b);
            linkgauche.zoom(6,5);
            linkgauche.ajouterImage(785, 126, 805, 152);
            linkgauche.ajouterImage(808, 126, 828, 152);
            linkgauche.ajouterImage(833, 126, 853, 152);
            linkgauche.ajouterImage(855, 126, 875, 152);
            linkgauche.ajouterImage(879, 126, 899, 152);
            linkgauche.ajouterImage(903, 126, 923, 152);
            linkgauche.ajouterImage(925, 126, 945, 152);
            linkgauche.setBoucle(true);
            linkgauche.inverseCoordonnees();


            linkattaquegauche = new Clip(b);
            linkattaquegauche.zoom(6,5);
            linkattaquegauche.ajouterImage(689, 431, 720, 462);
            linkattaquegauche.ajouterImage(722, 431, 753, 462);
            linkattaquegauche.ajouterImage(755, 431, 786, 462);
            linkattaquegauche.ajouterImage(806, 431, 837, 462);
            linkattaquegauche.ajouterImage(858, 431, 889, 462);
            linkattaquegauche.ajouterImage(900, 431, 931, 462);
            linkattaquegauche.ajouterImage(945, 431, 976, 462);
            linkattaquegauche.ajouterImage(991, 431, 1022, 462);
            linkattaquegauche.ajouterImage(1031, 431, 1062, 462);
            linkattaquegauche.setBoucle(true);
            linkattaquegauche.inverseCoordonnees();


            //LINK BAS
            linkbas = new Clip(b);
            linkbas.zoom(6,5);
            linkbas.ajouterImage(99, 125, 119, 151);
            linkbas.ajouterImage(122, 125, 142, 151);
            linkbas.ajouterImage(146, 125, 166, 151);
            linkbas.ajouterImage(168, 125, 189, 151);
            linkbas.ajouterImage(192, 125, 212, 151);
            linkbas.ajouterImage(216, 125, 236, 151);
            linkbas.ajouterImage(239, 125, 259, 151);
            linkbas.setBoucle(true);


            linkattaquebas = new Clip(b);
            linkattaquebas.zoom(6,5);
            linkattaquebas.ajouterImage(223, 307, 258, 342);
            linkattaquebas.ajouterImage(223, 307, 258, 342);
            linkattaquebas.ajouterImage(223, 307, 258, 342);
            linkattaquebas.ajouterImage(223, 307, 258, 342);
            linkattaquebas.setBoucle(true);


            //LINK HAUT
            linkhaut = new Clip(b);
            linkhaut.zoom(6,5);
            linkhaut.ajouterImage(454, 132, 474, 155);
            linkhaut.ajouterImage(477, 132, 497, 155);
            linkhaut.ajouterImage(501, 132, 521, 155);
            linkhaut.ajouterImage(524, 132, 544, 155);
            linkhaut.ajouterImage(548, 132, 568, 155);
            linkhaut.ajouterImage(571, 132, 591, 155);
            linkhaut.ajouterImage(593, 132, 613, 155);
            linkhaut.setBoucle(true);


            linkattaquehaut = new Clip(b);
            linkattaquehaut.zoom(6,5);
            linkattaquehaut.ajouterImage(405, 302, 431, 340);
            linkattaquehaut.ajouterImage(436, 302, 462, 340);
            linkattaquehaut.ajouterImage(467, 302, 492, 340);
            linkattaquehaut.ajouterImage(498, 302, 498, 340);
            linkattaquehaut.ajouterImage(526, 302, 551, 340);
            linkattaquehaut.ajouterImage(551, 302, 576, 340);
            linkattaquehaut.ajouterImage(579, 302, 604, 340);
            linkattaquehaut.setBoucle(true);


        } catch (IOException e) {
            e.printStackTrace();
        }



        //LIFE
        try{
             /*permet de créer un flux*/
            InputStream inputStream = assetManager.open("HUD.png");

            /*option de l'image*/
            BitmapFactory.Options options= new BitmapFactory.Options();

             /*on ne gere pas les pixels transparents ici*/
            options.inPreferredConfig = Bitmap.Config.RGB_565;

            /*charger l'image dans les décors en gérant les options*/
            e= BitmapFactory.decodeStream(inputStream, null, options);
            life= new Clip(e);
            life.ajouterImage(178,15,221,21);
            life.zoom(6,5);

        } catch (IOException e) {
            e.printStackTrace();
        }



        //OBJET
        try{
             /*permet de créer un flux*/
            InputStream inputStream = assetManager.open("HUD.png");

            /*option de l'image*/
            BitmapFactory.Options options= new BitmapFactory.Options();

             /*on ne gere pas les pixels transparents ici*/
            options.inPreferredConfig = Bitmap.Config.RGB_565;

            /*charger l'image dans les décors en gérant les options*/
            f= BitmapFactory.decodeStream(inputStream, null, options);
            hude= new Clip(e);
            hude.ajouterImage(20,15,152,59);
            hude.zoom(6,5);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


//******************************************************/
//****************** Bouge le héros ******************/
//******************************************************/

    public void bouge(float deltatime) {

        //******************************************************/
        //*************** AFFICHAGE DEPLACEMENT ****************/
        //******************************************************/

        if (etat == 'd'){
            linkdroit.deplace();
            ax=ax+20;
            linkdroit.next();
        }

        if (etat == 'g'){
            linkgauche.deplace();
            ax=ax-20;
            linkgauche.next();

        }

        if (etat == 'b'){
            linkbas.deplace();
            ay=ay+20;
            linkbas.next();
        }

        if (etat == 'h'){
            linkhaut.deplace();
            ay=ay-20;
            linkhaut.next();
        }

        //******************************************************/
        //****************** AFFICHAGE ATTAQUE *****************/
        //******************************************************/

        //GAUCHE
        if (etat == 'z'){
            linkattaquegauche.deplace();
            linkattaquegauche.next();
        }

        //DROIT
        if (etat == 'v'){
            linkattaquedroit.deplace();
            linkattaquedroit.next();
        }

        //HAUT
        if (etat == 'w'){
            linkattaquehaut.deplace();
            linkattaquehaut.next();
        }

        //BAS
        if (etat == 'y'){
            linkattaquebas.deplace();
            linkattaquebas.next();
        }

    }


//******************************************************/
//****************** Affiche le héros ******************/
//******************************************************/


    public void afficher(Canvas canvas) {

        //******************************************************/
        //****************** AFFICHAGE ACTION ******************/
        //******************************************************/

        //si il va en bas
        if (etat == 'b') {
            linkbas.affiche(ax, ay, canvas);
        }

        //si il va à droite afficher le clip droit
        if (etat == 'd') {
            linkdroit.affiche(ax, ay, canvas);
        }

        //si il va à gauche afficher le clip gauche
        if (etat == 'g') {
            linkgauche.affiche(ax, ay, canvas);
        }

        //si il va à haut afficher le clip haut
        if (etat == 'h') {
            linkhaut.affiche(ax, ay, canvas);
        }

        //******************************************************/
        //****************** AFFICHAGE STATIC ******************/
        //******************************************************/

        //si il allait en bas
        if (etat == 'n') {
            linkbas.affiche(ax, ay, canvas);

        }

        //si il allait à droite afficher le clip droit
        if (etat == 'q') {
            linkdroit.affiche(ax, ay, canvas);
        }

        //si il allait à gauche afficher le clip gauche
        if (etat == 'c') {
            linkgauche.affiche(ax, ay, canvas);
        }

        //si il allait à haut afficher le clip haut
        if (etat == 'x') {
            linkhaut.affiche(ax, ay, canvas);
        }


        //******************************************************/
        //*********************** ATTAQUE **********************/
        //******************************************************/

        //GAUCHE
        if (etat == 'z'){
            linkattaquegauche.affiche(ax, ay, canvas);
            linkattaquegauche.next();

        }

        //DROITE
        if (etat == 'v'){
            linkattaquedroit.affiche(ax, ay, canvas);
            linkattaquedroit.next();
        }

        //HAUT
        if (etat == 'w'){
            linkattaquehaut.affiche(ax, ay, canvas);
            linkattaquehaut.next();
        }

        //BAS
        if (etat == 'y'){
            linkattaquebas.affiche(ax, ay, canvas);
            linkattaquebas.next();
        }

        life.affiche(1600,30,canvas);
        hude.affiche(40,30,canvas);
    }

//******************************************************/
//******************* Touches appuyées *****************/
//******************************************************/

    //GAUCHE
    public void gaucheDown() {
        linkgauche.deplace();
        etat = 'g';

    }

    //DROITE
    public void droiteDown() {
        linkdroit.deplace();
        etat = 'd';
    }

    //HAUT
    public void hautDown() {
        etat ='h';
        linkhaut.deplace();
    }

    //BAS
    public void basDown() {
        linkbas.deplace();
        etat ='b';
    }





//******************************************************/
//****************** BOUTON "A" APPUYÉ *****************/
//******************************************************/
    public void coupUp() {
        //GAUCHE
        if (etat == 'z') {
            etat = 'c';
        }

        //DROITE
        if (etat == 'v') {
            etat = 'q';
        }

        //HAUT
        if (etat == 'w') {
            etat= 'x';
        }

        //BAS
        if (etat == 'y'){
            etat= 'n';
        }
    }


    public void coupDown() {

        //GAUCHE
        if (etat == 'c') {
            etat = 'z';
        }

        if (etat == 'g') {
            etat = 'z';
        }


        //DROITE
        if (etat == 'q') {
            etat = 'v';
        }

        if (etat == 'd') {
            etat = 'v';
        }

        //HAUT
        if (etat == 'x') {
            etat = 'w';
        }

        if (etat == 'h') {
            etat = 'w';
        }

        //BAS
        if (etat == 'n') {
            etat = 'y';
        }

        if (etat == 'b') {
            etat = 'y';
        }

    }

//******************************************************/
//******************* Touches relevées *****************/
//******************************************************/

    //GAUCHE
    public void gaucheUp() {
        etat = 'c';
    }

    //DROITE
    public void droiteUp() {

        etat = 'q';
    }

    //HAUT
    public void hautUp() {

        etat = 'x';
    }

    //BAS
    public void basUp() {

        etat = 'n';
    }


/******************************************************/

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

}
