package com.example.eleve.ppe2;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.example.eleve.moteur.RenderView;

// MainActivity est la classe qui se lance au démarrage du programme
public class MainActivity extends Activity implements View.OnTouchListener, View.OnKeyListener {
    RenderView renderView;
    PowerManager.WakeLock wakeLock;

    MediaPlayer mySound;

    // onCreate se lance lors du démarrage
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Supprimer l'affichage du titre de l'application à l'écran
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Met en plein écran l'application
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // Met en mémoire la zone de rendu
        renderView = new RenderView(this);
        // Orientation en portrait
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        // Le renderView va écouter les événements concernant le clavier
        renderView.setOnKeyListener(this);
        renderView.setFocusableInTouchMode(true);
        // Le renderView va écouter les événements concernant le tactile
        renderView.setOnTouchListener(this);
        // Donne à la fenêtre la zone de rendu
        renderView.requestFocus();
        setContentView(renderView);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mySound = MediaPlayer.create(this, R.raw.overworld);
    }



    @Override
    protected void onResume() {
        super.onResume();
        // Lorsque la fonction est en action
        renderView.resume();
        mySound.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Lance la pause du jeu
        renderView.pause();
        mySound.pause();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float ptX = event.getX();
        float ptY = event.getY();
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                renderView.toucherDown(ptX, ptY);
            break;
            case MotionEvent.ACTION_UP:
                renderView.toucherUp(ptX, ptY);
                break;

        }
        return true;
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        renderView.clavier(event);
        return true;
    }
}
