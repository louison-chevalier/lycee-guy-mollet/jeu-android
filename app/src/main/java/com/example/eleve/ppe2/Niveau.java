package com.example.eleve.ppe2;

import android.view.KeyEvent;

import com.example.eleve.moteur.RenderView;

/**
 * Created by julienlegales on 19/02/2017.
 */

// La classe niveau permet de déclarer toutes les méthodes qui existent pour chaque niveau
public abstract class Niveau {
    // Tous les niveaux doivent être initialiser
    public abstract void initialiser();
    // Tous les niveaux feront bouger des éléments
    public abstract void bouger(float deltatime);
    // Tous les niveaux vont être afficher
    public abstract void afficher();
    // Tous les niveaux vont gérer l'appuie sur une touche
    public abstract void toucherDown(float x, float y);
    // Tous les niveaux vont gérer le relâchement d'une touche
    public abstract void toucherUp(float x, float y);
    // Tous les niveaux gèrent le clavier
    public abstract void clavier(KeyEvent event);
    // Tous les ninveaux gèrent un coefficient en x
    public abstract float getCoeffx();
    // Tous les niveaux gèrent un coefficient en y
    public abstract float getCoeffy();
    // Tous les niveaux vont avoir un héros
    public abstract Heros getHeros();
    public abstract Barrevie getBarrevie();
    public abstract void setBarrevie(Barrevie barrevie);



/*
    public abstract RenderView getRenderView();
    public abstract void setRenderView(RenderView renderView);*/





}
